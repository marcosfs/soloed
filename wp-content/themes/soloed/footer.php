<footer id="contato">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <img src="<?php bloginfo('template_url') ?>/img/logo.png" alt="SoloED">
                <h4>+55 <?= get_post_meta(31, 'telefone', true) ?></h4>
                <p><a href="mailto:<?= get_post_meta(31, 'email', true) ?>"><?= get_post_meta(31, 'email', true) ?></a></p>
            </div>
            <div class="col-md-6"><strong>Links</strong>
                <?php wp_nav_menu(array('theme_location' => 'footer_menu', 'menu_class' => 'menu_footer')); ?>
            </div>
            <div class="col-md-3">
                <strong>Newsletter</strong>
                <form action="" method="post">
                    <div class="input-group mb-3">
                        <input type="email" class="form-control" name="email" placeholder="Receba atualizações" aria-label="Receba atualizações" aria-describedby="basic-addon2" required>
                        <div class="input-group-append">
                            <button class="btn btn-success" type="submit" name="enviar">&#x279C;</button>
                        </div>
                    </div>
                </form> 

                <?php
                    if(isset($_POST['enviar'])) :
                        $email = add_post_meta( 45, 'newsletter', $_POST['email'] );
                        if($email) :
                            echo "<div class='alert alert-success'>E-mail enviado com Sucesso!</div>";
                        endif;
                    endif;
                ?>

            </div>
        </div>
        <hr>
        <div class="row info">
            <div class="col-md-4">
                <a href="<?= get_post_meta(31, 'linkedin', true) ?>" target="_blank">
                    <img src="<?php bloginfo('template_url') ?>/img/linkedin.png" alt="">
                </a>

                <a href="<?= get_post_meta(31, 'facebook', true) ?>" target="_blank">
                    <img src="<?php bloginfo('template_url') ?>/img/facebook.png" alt="">
                </a>

                <a href="<?= get_post_meta(31, 'twitter', true) ?>" target="_blank">
                    <img src="<?php bloginfo('template_url') ?>/img/twitter.png" alt="">
                </a>
            </div>
            <div class="col-md-4 text-center">A product of <strong>SoloED</strong></div>
            <div class="col-md-4 text-end">© <?= date('Y') ?> Lift Media. All rights reserved</div>
        </div>

    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>