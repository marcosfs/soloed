<section class="servicos" id="servicos">
    <div class="container">
        <h2>Serviços/Qualidades</h2>
        <br>
        <p style="color: #2F2E41;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. <br>Lorem Ipsum has been the industry's standard dummy text ever since</p>
        <br>
        <div class="row">

        <?php
            $args = array(
                'post_type'      => 'servicos'
            );

            $loop = new WP_Query($args);
            while ( $loop->have_posts() ) {
                $loop->the_post();
                ?>
                    <div class="col-md-4">
                        <div class="box_servicos">
                            <?php the_post_thumbnail() ?>
                            <h3><?php the_title(); ?></h3>
                            <?php the_content(); ?>
                        </div>
                    </div>
                <?php } ?>
        </div>
    </div>
</section>