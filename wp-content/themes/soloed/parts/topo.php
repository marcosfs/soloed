<section class="topo">
    <div class="container">
        <div class="row texto_topo">

        <?php

        $args = array(
            'post_type'      => 'topo',
            'post_per_page'  => 1
        );

        $loop = new WP_Query($args);
            while ( $loop->have_posts() ) {
                $loop->the_post();
                ?>

                <div class="col-md-6 col-12">
                    <h2><?php the_title(); ?></h2>
                    <p><?php the_content(); ?></p>
                    <p><button class="btn btn-success"><?= get_post_meta(get_the_ID(), 'botao_planos', true) ?></button></p>

                </div>

                <div class="col-md-6 col-12">
                    <?php the_post_thumbnail('large', array('class'=>'img-fluid imagem_topo')); ?>
                </div>

                <?php } ?>

        </div>
    </div>
</section>