<section class="sobre_nos" id="sobre_nos">
    <div class="row">

    <?php
            $args = array(
                'post_type'      => 'sobre_nos',
                'post_per_page'  => 1
            );

            $loop = new WP_Query($args);
            while ( $loop->have_posts() ) {
                $loop->the_post();
                ?>

                <div class="col-md-6 col-12">
                  <?php the_post_thumbnail('large', array('class'=>'img-fluid imagem_sobre_nos')); ?>
                </div>

                <div class="col-md-6 col-12">
                    <h2><?php the_title(); ?></h3>
                    <p><?php the_content(); ?></p>
                </div>

                <?php } ?>
    </div>
</section>