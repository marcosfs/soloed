<?php get_header(); ?>

<main>

<?php
    get_template_part('parts/topo', 'topo');
    get_template_part('parts/sobre_nos', 'sobre_nos');
    get_template_part('parts/servicos', 'servicos');
    get_template_part('parts/depoimentos', 'depoimentos');
?>

</main>

<?php get_footer(); ?>