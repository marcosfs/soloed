<?php

// carregando folhas de estilos e js
function load_scripts(){
    wp_enqueue_script( 'bootstrap-js', 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.min.js', array(), '5.1', true);
    wp_enqueue_style( 'bootstrap-css', 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css', array(), '5.1', 'all');
    wp_enqueue_style( 'template', get_template_directory_uri() . '/css/template.css', array(), '1.34', 'all');
}

add_action('wp_enqueue_scripts', 'load_scripts');

// Registrar menu
register_nav_menus(
    array(
        'main_menu' => 'Menu Principal',
        'footer_menu' => 'Menu Footer'
    )
);


function adicionando_recursos(){
    // add_theme_support('custom-logo');
    add_theme_support('post-thumbnails');
}

add_action('after_setup_theme', 'adicionando_recursos');


add_action( 'init', 'topo' );
function topo() {
	$labels = array(
		"name" => "Topo",
		"singular_name" => "Topo",
		"menu_name" => "Topo",
		"all_items" => "Topo",
		);

	$args = array(
		"labels" => $labels,
		"description" => "Sobre Nós",
		"public" => true,
		"show_ui" => true,
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "topo", "with_front" => true ),
		"query_var" => true,
		"menu_icon"   => 'dashicons-upload',
		"taxonomies"  => array( ),
		"supports" => array( "title", "editor", "custom-fields", "thumbnail"),
	);
	register_post_type( "topo", $args );
}


add_action( 'init', 'sobre_nos' );
function sobre_nos() {
	$labels = array(
		"name" => "Sobre Nós",
		"singular_name" => "Sobre Nós",
		"menu_name" => "Sobre Nós",
		"all_items" => "Sobre Nós",
		);

	$args = array(
		"labels" => $labels,
		"description" => "Sobre Nós",
		"public" => true,
		"show_ui" => true,
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "sobre_nos", "with_front" => true ),
		"query_var" => true,
		"menu_icon"   => 'dashicons-admin-users',
		"taxonomies"  => array( ),
		"supports" => array( "title", "editor", "thumbnail"),
	);
	register_post_type( "sobre_nos", $args );
}


add_action( 'init', 'servicos' );
function servicos() {
	$labels = array(
		"name" => "Serviços",
		"singular_name" => "Serviço",
		"menu_name" => "Serviços",
		"all_items" => "Serviços",
		);

	$args = array(
		"labels" => $labels,
		"description" => "Serviços",
		"public" => true,
		"show_ui" => true,
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "servicos", "with_front" => true ),
		"query_var" => true,
		"menu_icon"   => 'dashicons-screenoptions',
		"taxonomies"  => array( ),
		"supports" => array( "title", "editor", "thumbnail"),
	);
	register_post_type( "servicos", $args );
}


add_action( 'init', 'rodape' );
function rodape() {
	$labels = array(
		"name" => "Rodapé",
		"singular_name" => "Rodapé",
		"menu_name" => "Rodapé",
		"all_items" => "Rodapé",
		);

	$args = array(
		"labels" => $labels,
		"description" => "Rodapé",
		"public" => true,
		"show_ui" => true,
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "rodape", "with_front" => true ),
		"query_var" => true,
		"menu_icon"   => 'dashicons-facebook',
		"taxonomies"  => array( ),
		"supports" => array("title", "custom-fields"),
	);
	register_post_type( "rodape", $args );
}


add_action( 'init', 'newsletter' );
function newsletter() {
	$labels = array(
		"name" => "Newsletter",
		"singular_name" => "Newsletter",
		"menu_name" => "Newsletter",
		"all_items" => "Newsletter",
		);

	$args = array(
		"labels" => $labels,
		"description" => "Newsletter",
		"public" => true,
		"show_ui" => true,
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "newsletter", "with_front" => true ),
		"query_var" => true,
		"menu_icon"   => 'dashicons-email',
		"taxonomies"  => array( ),
		"supports" => array("title", "custom-fields"),
	);
	register_post_type( "newsletter", $args );
}