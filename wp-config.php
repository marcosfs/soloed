<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'soloed' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', 'root' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '4JQLSdRLPG4cZ+3v*5kF6Vk~@n}Px:B!nNjQFy =~N6UuahW#.lM +M~rC _4C]W' );
define( 'SECURE_AUTH_KEY',  '24LVA&TDc8;E)[,OPu#m^<gMN4Po* s0?}YF6>Q/.H&*Jv[RN5V7!^>D`5Sk2S+i' );
define( 'LOGGED_IN_KEY',    'LxDW?,{k|r6Edr!5#8A9(~rWooAL`xpK&qmw,Ze&D8wGOam[/0UgHF=qvj@+G,_g' );
define( 'NONCE_KEY',        '3lhIT__u@yFvI7>>$t>$KDc5mGeU+@H,d$_(b]J+(slpd#.IAqb>wJr%kk?t$5A6' );
define( 'AUTH_SALT',        'bGh7x!{73mmbsS.^^6,+0FtDCc`U6SwuS-]?tcQcRkTXZt[?`nT5=O#v[C;4}P//' );
define( 'SECURE_AUTH_SALT', 'AUHGs[WaQ3Jxx,+D);A+k/m+ +rle`A$Jovc*9m`s@df|ZIe%qXA.tjC+r_Ih?>e' );
define( 'LOGGED_IN_SALT',   '6U/6=G(@sN6s-&[NeVycM9{GX[^q-ZT`BYTqL@yHj.%4~r6;-U(+<0`@z<p{GMbh' );
define( 'NONCE_SALT',       '[)WnQ9J5nox&en$i5)|/+7}|JZ:qV&5?0jq a:SyMHy.?#E!*)^88!y|Wh,Ug~h[' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';
